import { ArrayUtils } from '@feng3d/polyfill';
import { deepEqual, ok } from 'assert';
import { Queue } from '../src';

describe('Queue', () =>
{
    it('isEmpty', () =>
    {
        const q = new Queue<number>();

        deepEqual(q.isEmpty(), true);
    });

    it('empty', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const q = new Queue<number>();
        arr.forEach((element) =>
        {
            q.enqueue(element);
        });
        q.empty();
        deepEqual(q.isEmpty(), true);
    });

    it('peek', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const q = new Queue<number>();
        arr.forEach((element) =>
        {
            q.enqueue(element);
        });
        deepEqual(q.peek(), arr[0]);
    });

    it('enqueue', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const q = new Queue<number>();
        arr.forEach((element) =>
        {
            q.enqueue(element);
        });
        deepEqual(q.peek(), arr[0]);
    });

    it('enqueue', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const q = new Queue<number>();
        arr.forEach((element) =>
        {
            q.enqueue(element);
        });
        while (!q.isEmpty())
        {
            deepEqual(q.dequeue(), arr.shift());
        }
    });

    it('toString', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const q = new Queue<number>();
        arr.forEach((element) =>
        {
            q.enqueue(element);
        });

        ok(true, q.toString((v) => v.toFixed(3)));
    });
});
