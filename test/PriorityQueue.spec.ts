import { ArrayUtils } from '@feng3d/polyfill';
import { deepEqual } from 'assert';
import { PriorityQueue } from '../src';

describe('PriorityQueue', () =>
{
    it('push', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const compare = (a: number, b: number) => a - b;
        const q = new PriorityQueue(compare);
        q.push(...arr);

        let sortarr = arr.concat().sort(compare);
        deepEqual(q.toArray(), sortarr);

        arr.push(1);
        q.push(1);
        sortarr = arr.concat().sort();
        deepEqual(q.toArray(), sortarr);
    });

    it('shift', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const compare = (a: number, b: number) => a - b;
        const q = new PriorityQueue(compare);
        q.push(...arr);

        const sortarr = arr.concat().sort(compare);

        for (let i = sortarr.length - 1; i >= 0; i--)
        {
            deepEqual(q.shift(), sortarr.shift());
        }
    });

    it('toArray', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const compare = (a: number, b: number) => a - b;
        const q = new PriorityQueue(compare);
        q.push(...arr);

        const sortarr = arr.concat().sort(compare);

        deepEqual(q.toArray(), sortarr);
    });

    it('fromArray', () =>
    {
        const arr = ArrayUtils.create(10, () => Math.random());

        const compare = (a: number, b: number) => a - b;
        const q = new PriorityQueue(compare);
        q.fromArray(arr);

        const sortarr = arr.concat().sort(compare);
        deepEqual(q.toArray(), sortarr);
    });
});
