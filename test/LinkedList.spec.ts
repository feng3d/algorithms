import { ArrayUtils } from '@feng3d/polyfill';
import { deepEqual, ok } from 'assert';
import { LinkedList } from '../src';

describe('LinkedList', () =>
{
    it('LinkedList', () =>
    {
        const ll = new LinkedList<number>();
        deepEqual(ll.deleteHead(), null);
        deepEqual(ll.deleteTail(), null);

        ok(ll.checkStructure());
    });

    it('addHead', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        arr.concat().reverse().forEach((element) =>
        {
            ll.addHead(element);
        });

        deepEqual(ll.toArray(), arr);

        ll.addHead(1);
        arr.unshift(1);
        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('addTail', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        arr.forEach((element) =>
        {
            ll.addTail(element);
        });

        deepEqual(ll.toArray(), arr);

        ll.addTail(1);
        arr.push(1);
        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('delete', () =>
    {
        const ll = new LinkedList<number>();

        let arr = ArrayUtils.create(10, () => Math.random());
        arr = arr.concat(arr);
        arr.forEach((element) =>
        {
            ll.addTail(element);
        });

        deepEqual(ll.toArray(), arr);

        const deleteItem = arr[3];
        arr.splice(arr.indexOf(deleteItem), 1);

        ll.delete(deleteItem);

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('deleteAll', () =>
    {
        const ll = new LinkedList<number>();

        let arr = ArrayUtils.create(10, () => Math.random());
        arr = arr.concat(arr);
        arr.forEach((element) =>
        {
            ll.addTail(element);
        });

        deepEqual(ll.toArray(), arr);

        const deleteItem = arr[3];

        let index = arr.indexOf(deleteItem);
        while (index !== -1)
        {
            arr.splice(index, 1);
            index = arr.indexOf(deleteItem);
        }
        ll.deleteAll(deleteItem);

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('deleteHead', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        deepEqual(ll.deleteHead(), arr.shift());
        deepEqual(ll.deleteHead(), arr.shift());

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('deleteTail', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        deepEqual(ll.deleteTail(), arr.pop());
        deepEqual(ll.deleteTail(), arr.pop());

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('toArray', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('fromArray', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });

    it('toString', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        ok(true, ll.toString((v) => v.toFixed(3)));

        ok(ll.checkStructure());
    });

    it('reverse', () =>
    {
        const ll = new LinkedList<number>();

        const arr = ArrayUtils.create(10, () => Math.random());
        ll.fromArray(arr);

        ll.reverse();
        arr.reverse();

        deepEqual(ll.toArray(), arr);

        arr.length = 1;
        ll.fromArray(arr);

        ll.reverse();
        arr.reverse();

        deepEqual(ll.toArray(), arr);

        ok(ll.checkStructure());
    });
});
