import { deepEqual } from 'assert';
import { DisjointSet, DisjointSetNode } from '../src';

describe('DisjointSet', () =>
{
    it('should throw error when trying to union and check not existing sets', () =>
    {
        function mergeNotExistingSets()
        {
            const disjointSet = new DisjointSet();

            disjointSet.union('A', 'B');
        }

        function checkNotExistingSets()
        {
            const disjointSet = new DisjointSet();

            disjointSet.inSameSet('A', 'B');
        }

        let error0 = false; let
            error1 = false;
        try
        {
            mergeNotExistingSets();
        }
        catch (error)
        {
            error0 = true;
        }
        try
        {
            checkNotExistingSets();
        }
        catch (error)
        {
            error1 = true;
        }

        deepEqual(error0, true);
        deepEqual(error1, true);
    });

    it('should do basic manipulations on disjoint set', () =>
    {
        const disjointSet = new DisjointSet();

        deepEqual(disjointSet.find('A'), null);
        deepEqual(disjointSet.find('B'), null);

        disjointSet.makeSet('A');

        deepEqual(disjointSet.find('A'), 'A');
        deepEqual(disjointSet.find('B'), null);

        disjointSet.makeSet('B');

        deepEqual(disjointSet.find('A'), 'A');
        deepEqual(disjointSet.find('B'), 'B');

        disjointSet.makeSet('C');

        deepEqual(disjointSet.inSameSet('A', 'B'), false);

        disjointSet.union('A', 'B');

        deepEqual(disjointSet.find('A'), 'A');
        deepEqual(disjointSet.find('B'), 'A');
        deepEqual(disjointSet.inSameSet('A', 'B'), true);
        deepEqual(disjointSet.inSameSet('B', 'A'), true);
        deepEqual(disjointSet.inSameSet('A', 'C'), false);

        disjointSet.union('A', 'A');

        disjointSet.union('B', 'C');

        deepEqual(disjointSet.find('A'), 'A');
        deepEqual(disjointSet.find('B'), 'A');
        deepEqual(disjointSet.find('C'), 'A');

        deepEqual(disjointSet.inSameSet('A', 'B'), true);
        deepEqual(disjointSet.inSameSet('B', 'C'), true);
        deepEqual(disjointSet.inSameSet('A', 'C'), true);

        disjointSet
            .makeSet('E')
            .makeSet('F')
            .makeSet('G')
            .makeSet('H')
            .makeSet('I');

        disjointSet
            .union('E', 'F')
            .union('F', 'G')
            .union('G', 'H')
            .union('H', 'I');

        deepEqual(disjointSet.inSameSet('A', 'I'), false);
        deepEqual(disjointSet.inSameSet('E', 'I'), true);

        disjointSet.union('I', 'C');

        deepEqual(disjointSet.find('I'), 'E');
        deepEqual(disjointSet.inSameSet('A', 'I'), true);
    });

    it('should union smaller set with bigger one making bigger one to be new root', () =>
    {
        const disjointSet = new DisjointSet();

        disjointSet
            .makeSet('A')
            .makeSet('B')
            .makeSet('C')
            .union('B', 'C')
            .union('A', 'C');

        deepEqual(disjointSet.find('A'), 'B');
    });

    it('should do basic manipulations on disjoint set with custom key extractor', () =>
    {
        const keyExtractor = (value) => value.key;

        const disjointSet = new DisjointSet(keyExtractor);

        const itemA = { key: 'A', value: 1 };
        const itemB = { key: 'B', value: 2 };
        const itemC = { key: 'C', value: 3 };

        deepEqual(disjointSet.find(itemA), null);
        deepEqual(disjointSet.find(itemB), null);

        disjointSet.makeSet(itemA);

        deepEqual(disjointSet.find(itemA), 'A');
        deepEqual(disjointSet.find(itemB), null);

        disjointSet.makeSet(itemB);

        deepEqual(disjointSet.find(itemA), 'A');
        deepEqual(disjointSet.find(itemB), 'B');

        disjointSet.makeSet(itemC);

        deepEqual(disjointSet.inSameSet(itemA, itemB), false);

        disjointSet.union(itemA, itemB);

        deepEqual(disjointSet.find(itemA), 'A');
        deepEqual(disjointSet.find(itemB), 'A');
        deepEqual(disjointSet.inSameSet(itemA, itemB), true);
        deepEqual(disjointSet.inSameSet(itemB, itemA), true);
        deepEqual(disjointSet.inSameSet(itemA, itemC), false);

        disjointSet.union(itemA, itemC);

        deepEqual(disjointSet.find(itemA), 'A');
        deepEqual(disjointSet.find(itemB), 'A');
        deepEqual(disjointSet.find(itemC), 'A');

        deepEqual(disjointSet.inSameSet(itemA, itemB), true);
        deepEqual(disjointSet.inSameSet(itemB, itemC), true);
        deepEqual(disjointSet.inSameSet(itemA, itemC), true);
    });
});

describe('DisjointSetNode', () =>
{
    it('should do basic manipulation with disjoint set item', () =>
    {
        const itemA = new DisjointSetNode('A');
        const itemB = new DisjointSetNode('B');
        const itemC = new DisjointSetNode('C');
        const itemD = new DisjointSetNode('D');

        deepEqual(itemA.getRank(), 0);
        deepEqual(itemA.getChildren(), []);
        deepEqual(itemA.getKey(), 'A');
        deepEqual(itemA.getRoot(), itemA);
        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), true);

        itemA.addChild(itemB);
        itemD.setParent(itemC);

        deepEqual(itemA.getRank(), 1);
        deepEqual(itemC.getRank(), 1);

        deepEqual(itemB.getRank(), 0);
        deepEqual(itemD.getRank(), 0);

        deepEqual(itemA.getChildren().length, 1);
        deepEqual(itemC.getChildren().length, 1);

        deepEqual(itemA.getChildren()[0], itemB);
        deepEqual(itemC.getChildren()[0], itemD);

        deepEqual(itemB.getChildren().length, 0);
        deepEqual(itemD.getChildren().length, 0);

        deepEqual(itemA.getRoot(), itemA);
        deepEqual(itemB.getRoot(), itemA);

        deepEqual(itemC.getRoot(), itemC);
        deepEqual(itemD.getRoot(), itemC);

        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), false);
        deepEqual(itemC.isRoot(), true);
        deepEqual(itemD.isRoot(), false);

        itemA.addChild(itemC);

        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), false);
        deepEqual(itemC.isRoot(), false);
        deepEqual(itemD.isRoot(), false);

        deepEqual(itemA.getRank(), 3);
        deepEqual(itemB.getRank(), 0);
        deepEqual(itemC.getRank(), 1);
    });

    it('should do basic manipulation with disjoint set item with custom key extractor', () =>
    {
        const keyExtractor = (value) =>
            value.key;

        const itemA = new DisjointSetNode({ key: 'A', value: 1 }, keyExtractor);
        const itemB = new DisjointSetNode({ key: 'B', value: 2 }, keyExtractor);
        const itemC = new DisjointSetNode({ key: 'C', value: 3 }, keyExtractor);
        const itemD = new DisjointSetNode({ key: 'D', value: 4 }, keyExtractor);

        deepEqual(itemA.getRank(), 0);
        deepEqual(itemA.getChildren(), []);
        deepEqual(itemA.getKey(), 'A');
        deepEqual(itemA.getRoot(), itemA);
        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), true);

        itemA.addChild(itemB);
        itemD.setParent(itemC);

        deepEqual(itemA.getRank(), 1);
        deepEqual(itemC.getRank(), 1);

        deepEqual(itemB.getRank(), 0);
        deepEqual(itemD.getRank(), 0);

        deepEqual(itemA.getChildren().length, 1);
        deepEqual(itemC.getChildren().length, 1);

        deepEqual(itemA.getChildren()[0], itemB);
        deepEqual(itemC.getChildren()[0], itemD);

        deepEqual(itemB.getChildren().length, 0);
        deepEqual(itemD.getChildren().length, 0);

        deepEqual(itemA.getRoot(), itemA);
        deepEqual(itemB.getRoot(), itemA);

        deepEqual(itemC.getRoot(), itemC);
        deepEqual(itemD.getRoot(), itemC);

        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), false);
        deepEqual(itemC.isRoot(), true);
        deepEqual(itemD.isRoot(), false);

        itemA.addChild(itemC);

        deepEqual(itemA.isRoot(), true);
        deepEqual(itemB.isRoot(), false);
        deepEqual(itemC.isRoot(), false);
        deepEqual(itemD.isRoot(), false);

        deepEqual(itemA.getRank(), 3);
        deepEqual(itemB.getRank(), 0);
        deepEqual(itemC.getRank(), 1);
    });
});
